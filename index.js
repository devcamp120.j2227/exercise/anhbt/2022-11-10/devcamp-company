const express = require("express");
const app = express();

const companyRouter = require("./app/routes/companyRouter")
app.use("/", companyRouter);

const port = 8000;
app.listen(port, function() {
    console.log(`App running on port ${port}`);
})