const getAllCompanyMiddleware = function(request, response, next) {
    console.log(`get ALL company`);
    next();
}

const createCompanyMiddleware = function(request, response, next) {
    console.log(`create company`);
    next();
}

const getDetailCompanyMiddleware = function(request, response, next) {
    console.log(`get detail company`);
    next();
}

const updateCompanyMiddleware = function(request, response, next) {
    console.log(`update company`);
    next();
}

const deleteCompanyMiddleware = function(request, response, next) {
    console.log(`delete company`);
    next();
}

module.exports = {
    getAllCompanyMiddleware,
    createCompanyMiddleware,
    getDetailCompanyMiddleware,
    getAllCompanyMiddleware,
    updateCompanyMiddleware,
    deleteCompanyMiddleware
}