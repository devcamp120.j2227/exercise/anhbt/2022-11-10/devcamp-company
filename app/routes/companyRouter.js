const express = require("express");
const router = express.Router();
const companyMiddleware = require("../middleware/companyMiddleware.js");

router.use(function(request, response, next) {
    console.log(`Request URL: ${request.url}`);
    next();
})

router.get("/companies", companyMiddleware.getAllCompanyMiddleware, function(request, response){
    class Company {
        constructor(id, company, contact, country) {
            this.id = id,
            this.company = company,
            this.contact = contact, 
            this.country = country
        }
    }
    let company1 = new Company (
        1,
        `Alfreds Futterkiste`,
        `Maria Anders`,
        `Germany`
        )
    let company2 = new Company (
        2,
        `Centro commercial Moctezuma`,
        `Francisco Chang`,
        `Mexico`        
        )
    let company3 = new Company (
        3,
        `Ernst Handel`,
        `Roland Mendel`,
        `Austria`
        )
    let company4 = new Company (
        4,
        `Island Trading`,
        `Helen Bennett`,
        `UK`
        )
    let company5 = new Company (
        5,
        `Laughing Bacchus Winecellars`,
        `Yoshi Tannamuri`,
        `Canada`  
        )
    let company6 = new Company (
        6,
        `Magazzini Alimentari Riuniti`,
        `Giovanni Rovelli`,
        `Italy`
        )
        
    response.json({
        companies: [company1, company2, company3, company4, company5, company6]
    })
})
router.post("/companies", companyMiddleware.createCompanyMiddleware, function(request, response){
    response.json({
        message: 'Create Company'
    })
})
router.get("/companies/:companyid", companyMiddleware.getDetailCompanyMiddleware , function(request, response){
    const companyId = request.params.companyId
    response.json({
        message: `Get Company with ID ${companyId}`
    })
})
router.put("/companies/:companyid", companyMiddleware.updateCompanyMiddleware, function(request, response){
    const companyId = request.params.companyId
    response.json({
        message: `Update Company with ID ${companyId}`
    })
})
router.delete("/companies/:companyid", companyMiddleware.deleteCompanyMiddleware, function(request, response){
    const companyId = request.params.companyId
    response.json({
        message: `Delete Company with ID ${companyId}`
    })
})

module.exports = router;